use piston::input::*;
use crate::app::Coords;
use crate::app::Obj;
use crate::app::fit_to_grid;
use crate::app::GRID_SIZE;

pub enum Direction {
	Up,
	Down,
	Left,
	Right,
}

pub struct Snake {
	pub direction: Direction,
	pub speed: f64,
	pub length: u32,
	pub body_vec: Vec<Obj>,
}

impl Snake {
	pub fn new() -> Self {
		let head = Obj {
			coords: Coords::new(0.0, 0.0),
			size: GRID_SIZE,
		};

		Snake {
			direction: Direction::Right,
			speed: 1.0,
			length: 1,
			body_vec: vec![head],
		}
	}

	// Change snake direction when key is pressed
	pub fn change_direction(&mut self, key: keyboard::Key) {
		self.direction = match key {
			Key::Up | Key::W => Direction::Up,
			Key::Down | Key::S => Direction::Down,
			Key::Right | Key::D => Direction::Right,
			Key::Left | Key::A => Direction::Left,
			_ => return,
		};
	}

	// Update snake direction
	pub fn update_pos(&mut self) {

		// Give snake body n+1 the coords of n
		if self.body_vec.len() > 1 {
			let mut body_vec_new: Vec<Obj> = Vec::with_capacity(self.body_vec.len());
			body_vec_new.push(self.body_vec[0]);
			for i in 0..self.body_vec.len() {
				if i == 0 {
					continue;
				}

				body_vec_new.push(self.body_vec.get(i-1).unwrap().to_owned());
			}

			self.body_vec = body_vec_new;
		}

		// Update the coords of the snake head
		match self.direction {
			Direction::Up => { self.body_vec[0].coords.y = fit_to_grid(self.body_vec[0].coords.y - GRID_SIZE) },
			Direction::Down => { self.body_vec[0].coords.y = fit_to_grid(self.body_vec[0].coords.y + GRID_SIZE) },
			Direction::Right => { self.body_vec[0].coords.x = fit_to_grid(self.body_vec[0].coords.x + GRID_SIZE) },
			Direction::Left => { self.body_vec[0].coords.x = fit_to_grid(self.body_vec[0].coords.x - GRID_SIZE) },
		}
	}

	// Increment the snake body
	pub fn inc_body(&mut self) {
		
		macro_rules! body_direction {
			( x => $sign:tt ) => {
				Coords::new(
					self.body_vec[0].coords.x $sign GRID_SIZE,
					self.body_vec[0].coords.y
				)
			};

			( y => $sign:tt ) => {
				Coords::new(
					self.body_vec[0].coords.x,
					self.body_vec[0].coords.y $sign GRID_SIZE
				)
			}
		}

		// Clone coords of the head and move the body behind it
		let coords = match self.direction {
			Direction::Up => body_direction!(y => -),
			Direction::Down => body_direction!(y => +),
			Direction::Right => body_direction!(x => -),
			Direction::Left => body_direction!(x => +),
		};

		let body = Obj::new(coords.x, coords.y);
		self.body_vec.push(body);
	}
}
