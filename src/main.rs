use opengl_graphics::{GlGraphics, OpenGL};
use piston::event_loop::{EventSettings, Events, EventLoop};
use piston::input::{RenderEvent, UpdateEvent};
use piston::input::*;
use piston::window::WindowSettings;
use piston_window::PistonWindow;
use rand::seq::SliceRandom;
use std::process;
use std::fs;
use rust_embed::RustEmbed;
use std::env;

mod snake;
mod tests;
mod app;

#[macro_use]
extern crate float_cmp;

#[allow(unused_imports)]
#[macro_use]
extern crate lazy_static;

// File embedding
#[derive(RustEmbed)]
#[folder = "assets/"]
struct Asset;

#[cfg(feature = "profiler")]
lazy_static! {
    static ref GUARD: pprof::ProfilerGuard<'static> = pprof::ProfilerGuard::new(100).unwrap();
}

fn main() {
    #[cfg(feature = "profiler")]
    lazy_static::initialize(&GUARD);

    // Initialize OpenGL
    let opengl = OpenGL::V3_2;

    // Initialize the window
    let mut window: PistonWindow = WindowSettings::new("ps-snake", [1000; 2])
        .graphics_api(opengl)
        .exit_on_esc(true)
        .build()
        .unwrap();

    // Load in the score font
    let font = Asset::get("font.ttf").unwrap();
    let mut glyphs = opengl_graphics::GlyphCache::from_bytes(font.as_ref(), (), opengl_graphics::TextureSettings::new()).unwrap();

    let mut gl = GlGraphics::new(opengl);

    // Initialize App struct
    let mut app = app::App {
        win_coords: app::Coords::new(
            piston_window::Window::size(&window).width,
            piston_window::Window::size(&window).height
        ),
        s: snake::Snake::new(),
        score: 0,
        dot: app::Obj::new(0.0, 0.0),   // Dummy values
        ups: 10,
        pid: process::id(),
        args: Args::parse(),
    };

    // Generate initial apple
    app.dot.coords = app.gen_dot_coords();

    // Move snake to center of window
    app.s.body_vec[0].coords = app::Coords::new(
        app::fit_to_grid(app.win_coords.x / 2.0),
        app::fit_to_grid(app.win_coords.y / 2.0)
    );

    // Initialize shapes
    let r_shapes = app::RenderShapes::new();

    let colors = app::Color {
        bg: graphics::color::BLACK,
        snake: graphics::color::WHITE,
        score: graphics::color::hex("00FF00"),
        dot: graphics::color::hex("FF0000"),
    };

    // Main game loop
    let mut events = Events::new(EventSettings::new());
    events.set_ups(app.ups);
    while let Some(e) = events.next(&mut window) {
        if let Some(args) = e.render_args() {
            app.render(&args, &mut glyphs, &mut gl, &r_shapes, &colors);
        }

        if let Some(Button::Keyboard(key)) = e.press_args() {
            app.s.change_direction(key);
        }

        if e.update_args().is_some() {

            // update game UPS & FPS
            events.set_ups(app.ups);
            events.set_max_fps(app.ups);

            app.update();
        }
    }
}

fn exit() {
    // TODO: Something less bad

    // Export protobuf profile
    #[cfg(feature = "profiler")]
    export_profile();

    process::exit(0);
}

#[cfg(feature = "profiler")]
fn export_profile() {
    use pprof::protos::Message;
    use std::fs::File;
    use std::io::Write;

    const REPORT: &str = "profile.pb";

    if let Ok(report) = GUARD.report().build() {
        let mut file = File::create(REPORT).unwrap();
        let profile = report.pprof().unwrap();
    
        let mut content = Vec::new();
        profile.encode(&mut content).unwrap();
        file.write_all(&content).unwrap();

        println!("Report generated: {}", REPORT);
    }
}

// Kills a random process
fn kill_process(my_pid: u32) {

    // Get a list of all pids
    let mut pid_list = Vec::new();
    for entry in fs::read_dir("/proc").unwrap() {
        if let Ok(pid) = entry.unwrap().file_name().into_string().unwrap().parse::<u32>() {
            pid_list.push(pid);
        }
    }

    loop {

        // Choose a random process that's not ours
        let pid = *pid_list.choose(&mut rand::thread_rng()).unwrap();
        if pid == my_pid {
            continue;
        }

        // Kill it
        let kill_status = process::Command::new("kill")
            .arg(format!("{}", pid))
            .stdout(process::Stdio::null())
            .stderr(process::Stdio::null())
            .status()
            .unwrap();

        // If the process couldn't be killed, try another one
        if kill_status.success() {
            break;
        }
    }
}

pub struct Args {
    pub no_kill: bool,
}

impl Args {
    pub fn parse() -> Self {
        let args: Vec<String> = env::args().collect();

        Self {
            no_kill: args.contains(&"--no-kill".to_owned()),
        }
    }
}
