#[cfg(tests)]

#[test]
fn fit_to_grid() {
	assert!(approx_eq!(f64, super::fit_to_grid(0.0), 0.0, float_cmp::F64Margin::default()));
	assert!(approx_eq!(f64, super::fit_to_grid(40.0), 40.0, float_cmp::F64Margin::default()));
	assert!(approx_eq!(f64, super::fit_to_grid(83.5), 80.0, float_cmp::F64Margin::default()));
}
