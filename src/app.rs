use piston::input::*;
use rand::prelude::*;

use crate::snake;

pub const GRID_SIZE: f64 = 40.0;

// Game constructs
pub struct App {
    pub win_coords: Coords,
    pub s: snake::Snake,
    pub score: u64,
    pub dot: Obj,
    pub ups: u64,
    pub pid: u32,
    pub args: crate::Args,
}

impl App {

    // Renders objects
    pub fn render(
        &self,
        args: &RenderArgs,
        glyphs: &mut opengl_graphics::GlyphCache,
        gl: &mut opengl_graphics::GlGraphics,
        r_shapes: &RenderShapes,
        colors: &Color,
    ) {
        use graphics::*;

        // Draw objects onto screen
        gl.draw(args.viewport(), |c, gl| {
            clear(colors.bg, gl);

            macro_rules! draw {
                ( [$x:expr, $y:expr], $color:expr, $shape:expr ) => {
                    graphics::rectangle(
                        $color,
                        $shape,
                        c.transform.trans($x, $y),
                        gl
                    )
                };

                ( [$x:expr, $y:expr], $color:expr, $text:expr, $size:expr ) => {
                    graphics::text::Text::new_color($color, $size)
                        .draw(
                            $text,
                            glyphs,
                            &c.draw_state,
                            c.transform.trans($x, $y),
                            gl
                        )
                        .unwrap()
                };
            }

            // Draw dot
            draw!([self.dot.coords.x, self.dot.coords.y], colors.dot, r_shapes.dot);

            // Draw snake
            for snake in self.s.body_vec.iter() {
                draw!([snake.coords.x, snake.coords.y], colors.snake, r_shapes.snake);
            }

            // Draw score
            draw!([100.0, 100.0], colors.score, &format!("{}", self.score), 40);
        });
    }

    // Updates the game state
    pub fn update(&mut self) {

        // Handles wall collisions
        if self.is_collide(BoundsType::Border) {
            crate::exit();
        }

        // Handles collisions with snake body
        for (i, o) in self.s.body_vec.iter().enumerate() {
            if i == 0 {
                continue;
            }

            if self.is_collide(BoundsType::Obj(&o, &self.s.body_vec[0])) {
                crate::exit();
            }
        }

        // If a dot is consumed
        if self.is_collide(BoundsType::Obj(&self.dot, &self.s.body_vec[0])) {
            self.dot.coords = self.gen_dot_coords();
            self.update_score();
            self.s.inc_body();

            // Skips if --no-kill is enabled
            if !self.args.no_kill {
                crate::kill_process(self.pid);
            }
        }

        self.s.update_pos();
    }

    // Detects collisions
    fn is_collide(&self, b_type: BoundsType) -> bool {
        match b_type {

            // Wall collisions
            BoundsType::Border => {
                (self.s.body_vec[0].coords.y > self.win_coords.y) ||
                (self.s.body_vec[0].coords.y < 0.0) ||
                (self.s.body_vec[0].coords.x > self.win_coords.x) ||
                (self.s.body_vec[0].coords.x < 0.0)
            },

            // Object collisions
            BoundsType::Obj(o1, o2) => { o1.coords == o2.coords }
        }
    }

    fn update_score(&mut self) {
        self.score += 100;

        // Every 1000 points, increase the UPS by 3
        if self.score % 1000 == 0 {
            self.ups += 3;
        }
    }

    // Generate random dot coords
    pub fn gen_dot_coords(&self) -> Coords {

        let mut coords = Coords::new(0.0, 0.0);

        // Generate random coords until valid one is found
        'outer: loop {

            coords.x = fit_to_grid(rand::thread_rng().gen_range(0.0 + GRID_SIZE, self.win_coords.x - GRID_SIZE));
            coords.y = fit_to_grid(rand::thread_rng().gen_range(0.0 + GRID_SIZE, self.win_coords.y - GRID_SIZE));

            let dot = Obj::new(coords.x, coords.y);

            // Check if coords are within snake
            for snake in self.s.body_vec.iter() {

                // Generate new coords if collision exists
                if self.is_collide(BoundsType::Obj(&dot, &snake)) {
                   continue 'outer; 
                }
            }

            break;
        }

        // Valid coords have been found, return them
        coords
    }
}

#[derive(PartialEq, Clone, Copy)]
pub struct Obj {
	pub coords: Coords,
	pub size: f64,
}

impl Obj {
	pub fn new(x: f64, y: f64) -> Self {
		Self {
			coords: Coords::new(x, y),
			size: GRID_SIZE,
		}
	}
}

#[derive(Clone, Copy)]
pub struct Coords {
    pub x: f64,
    pub y: f64,
}

impl Coords {
    pub fn new(x: f64, y: f64) -> Self {
        Self { x, y }
    }
}

impl PartialEq for Coords {
    fn eq(&self, other: &Self) -> bool {
        approx_eq!(f64, self.x, other.x, float_cmp::F64Margin::default()) &&
        approx_eq!(f64, self.y, other.y, float_cmp::F64Margin::default())
    }
}

impl Eq for Coords {}

// Used for collision detection
enum BoundsType<'a> {
    Border,
    Obj(&'a Obj, &'a Obj),   
}

pub struct RenderShapes {
    pub snake: graphics::types::Rectangle,
    pub dot: graphics::types::Rectangle,
}

impl RenderShapes {
    pub fn new() -> Self {
        use graphics::*;

        Self {
            snake: rectangle::square(0.0, 0.0, GRID_SIZE),
            dot: rectangle::square(0.0, 0.0, GRID_SIZE),
        }
    }
}

pub struct Color {
    pub bg: graphics::types::Color,
    pub snake: graphics::types::Color,
    pub score: graphics::types::Color,
    pub dot: graphics::types::Color,
}

// Aligns coordinate to grid (40 pixels)
pub fn fit_to_grid(coord: f64) -> f64 {
    coord - (coord % GRID_SIZE)
}
